
<h2 class="ui center aligned icon header" style="color:#fafafa;margin: 0;">
    <i class="circular pin-account zmdi icon"></i>
</h2>
<div id="login-tab" class="ui secondary menu">
    <a class="item tooltip" title="修改密码" data-tab="change"><i class="key-minus large mdi icon"></i></a>
    <a class="item tooltip active" title="登录" data-tab="login"><i class="sign-in zmdi large icon"></i></a>
    <a class="item tooltip" title="重置密码" data-tab="reset"><i class="key-remove mdi large icon"></i></a>
</div>
<form id="change-form" action="${sri.makeUrlByType("changePassword", "transition", null, "false").getUrl()}" class="ui tab large form transition segment" data-tab="change">
    <div class="field">
        <div class="ui input">
            <input type="text" name="username" value="${(ec.getWeb().getErrorParameters().get("username"))!""}" placeholder="Username" required="required">
        </div>
    </div>
    <div class="field">
        <div class="ui input">
            <input type="password" name="oldPassword" placeholder="Old Password" required="required">
        </div>
    </div>
    <div class="field">
        <div class="ui input">
            <input type="password" name="newPassword" placeholder="New Password" required="required">
        </div>
    </div>
<#if !ec.getWeb()?? || ec.getWeb().getSession().getAttribute("moqui.tenantAllowOverride")! != "N">
    <div class="field">
        <div class="ui input">
            <input type="password" name="newPasswordVerify" placeholder="New Password Verify" required="required">
        </div>
    </div>
    <div class="field last">
        <div class="ui input">
            <input type="text" name="tenantId" placeholder="Tenant ID">
        </div>
    </div>
<#else>
    <div class="field last">
        <div class="ui input">
            <input type="password" name="newPasswordVerify" placeholder="New Password Verify" required="required">
        </div>
    </div>
</#if>
    <button class="ui circular mini submit icon basic disabled button" type="submit"><i class="arrow-right zmdi icon"></i></button>
</form>
<form id="login-form" class="ui tab large form active transition segment" data-tab="login">
    <div class="field">
        <div class="ui input">
            <input type="text" name="username" value="${(ec.getWeb().getErrorParameters().get("username"))!""}" placeholder="Username" required="required">
        </div>
    </div>
<#if !ec.getWeb()?? || ec.getWeb().getSession().getAttribute("moqui.tenantAllowOverride")! != "N">
    <div class="field">
        <div class="ui input">
            <input type="password" name="password" placeholder="Password" required="required">
        </div>
    </div>
    <div class="field last">
        <div class="ui input">
            <input type="text" name="tenantId" placeholder="Tenant ID">
        </div>
    </div>
<#else>
    <div class="field last">
        <div class="ui input">
            <input type="password" name="password" placeholder="Password" required="required">
        </div>
    </div>
</#if>
    <button class="ui circular mini submit icon basic disabled button" type="submit"><i class="arrow-right zmdi icon"></i></button>
</form>
<form id="reset-form" class="ui tab large form transition segment" data-tab="reset">
<#if !ec.getWeb()?? || ec.getWeb().getSession().getAttribute("moqui.tenantAllowOverride")! != "N">
    <div class="field">
        <div class="ui input">
            <input type="text" name="username" value="${(ec.getWeb().getErrorParameters().get("username"))!""}" placeholder="Username" required="required">
        </div>
    </div>
    <div class="field last">
        <div class="ui input">
            <input type="text" name="tenantId" placeholder="Tenant ID">
        </div>
    </div>
<#else>
    <div class="field last">
        <div class="ui input">
            <input type="text" name="username" value="${(ec.getWeb().getErrorParameters().get("username"))!""}" placeholder="Username" required="required">
        </div>
    </div>
</#if>
    <button class="ui circular mini submit icon basic disabled button" type="submit"><i class="arrow-right zmdi icon"></i></button>
</form>
<script>
    //tab 提示
    $('#login-tab .item').popup({
        variation:'inverted mini',
        position:'top center',
    });
    //tab
    $('#login-tab .item').tab({
        onVisible:function(tabPath){
            $('.ui.form.segment').removeClass('visible').addClass('hidden');
            if(!$('#'+tabPath+'-form').hasClass('hidden'))$('#'+tabPath+'-form').addClass('hidden');
            $('#'+tabPath+'-form').transition({
                animation:'scale',
                onComplete:function(){
                    $('#'+tabPath+'-form').find('input')[0].focus();
                }
            });
        }
    });
    //页面加载完毕显示登录页面
    $(function(){
        $('.column').transition({
            animation:'fade',
            duration:1000,
            onComplete:function(){

                $('#login-form').form({
                    fields: {
                        username : {identifier: 'username', rules: [{type   : 'empty', prompt : 'Please enter your name'}]},
                        password : {identifier: 'password', rules: [{type   : 'empty', prompt : 'Please enter your password'}]},
                    },
                    on:'change',
                    inline:true
                });
                $('#login-form').find('input')[0].focus();
                if($('#login-form').form('is valid'))$('#login-form').find('.ui.submit.button').removeClass('disabled')

                $('#change-form').form({
                    fields: {
                        username : {identifier: 'username', rules: [{type   : 'empty', prompt : 'Please enter your name'}]},
                        oldPassword : {identifier: 'oldPassword', rules: [{type   : 'empty', prompt : 'Please enter your password'}]},
                        newPassword : {identifier: 'newPassword', rules: [{type   : 'empty', prompt : 'Please enter your password'}]},
                        newPasswordVerify : {identifier: 'newPasswordVerify', rules: [{type   : 'empty', prompt : 'Please enter your password'},{type   : 'match[newPassword]', prompt : 'Please enter your password'}]},
                    },
                    on:'change',
                    inline:true
                });
                $('#reset-form').form({
                    fields: {
                        username : {identifier: 'username', rules: [{type   : 'empty', prompt : 'Please enter your name'}]},
                    },
                    on:'change',
                    inline:true
                });
                //提交按钮监视
                $('.ui.form.segment input').on('keyup',function(){
                    var parent = $(this).parents('.ui.tab.segment');
                    var button = parent.find('.ui.submit.button');
                    if(parent.form('is valid')){
                        button.removeClass('disabled');
                    }else{
                        button.addClass('disabled');
                    }
                });
            }
        });
    });
</script>
