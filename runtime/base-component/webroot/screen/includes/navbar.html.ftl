<div class="ui secondary menu">
    <#assign headerLogoList = sri.getThemeValues("STRT_HEADER_LOGO")>
    <#if headerLogoList?has_content><a href="${sri.buildUrl("/").getUrl()}" class="header item"><img src="${sri.buildUrl(headerLogoList?first).getUrl()}"></a></#if>
    <#assign headerTitleList = sri.getThemeValues("STRT_HEADER_TITLE")>
    <#if headerTitleList?has_content><div class="header item">${ec.resource.expand(headerTitleList?first, "")}</div></#if>
    <div class="item breadcrumb">
        <div id="header-menus" class="ui breadcrumb">
        </div>
    </div>
    <#--<div class="item">-->
        <#--<div id="navbar-menu-crumbs"></div>-->
    <#--</div>-->
    <div class="nav item">
    ${html_title!(ec.resource.expand(sri.screenUrlInfo.targetScreen.getDefaultMenuName()!"Page", ""))}
    </div>
    <div class="right menu">
    <#assign screenHistoryList = ec.web.getScreenHistory()>
        <div id="history-menus" class="item icon ui inline top right pointing dropdown">
            <i class="tv-list zmdi icon"></i>
            <div class="menu">
            <#list screenHistoryList as screenHistory><#if (screenHistory_index >= 25)><#break></#if>
            <a href="${screenHistory.url}" class="item">
                <#if screenHistory.image?has_content>
                    <#if screenHistory.imageType == "icon">
                        <i class="${screenHistory.image}"></i>
                    <#elseif screenHistory.imageType == "url-plain">
                        <img src="${screenHistory.image}" class="ui avatar image"/>
                    <#else>
                        <img src="${sri.buildUrl(screenHistory.image).url}"  class="ui avatar image"/>
                    </#if>
                <#else>
                    <i class="link zmdi icon"></i>
                </#if>
            ${screenHistory.name}
            </a>
            </#list>
            </div>
        </div>
        <a href="/Login/logout" title="Logout ${(ec.getUser().getUserAccount().userFullName)!!}"
           class="icon tooltip item">
            <i class="power red icon"></i>
        </a>
    <#assign navbarItemList = sri.getThemeValues("STRT_HEADER_NAVBAR_ITEM")>
    <#list navbarItemList! as navbarItem>
        <#assign navbarItemTemplate = navbarItem?interpret>
        <@navbarItemTemplate/>
    </#list>
    </div>
</div>
<script>
    $('#history-menus').dropdown({
        on:'hover',
    });
</script>

