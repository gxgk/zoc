## 试试做一下moqui
建议用intellij idea 开发项目
项目依赖:

postgresql数据库(当前版本:9.5)
gradle(当前版本2.10)    
jdk(当前版本1.8.65)

项目启动:

1.配置项目jdk
2.配置framework/src/main/resources/MoquiDefaultConf.xml 中数据库连接

datasource :transactional
datasource :tenantcommon(多租户)

3.创建 postgresql 数据库 (与数据库连接配置相同)

4.到 gradle 中之行 zoc/tasks/other/load 命令 加载数据,建表

5.到 Run/Debug - Edit Configurations - Add New Configurations - Add jar application
  path to jar 设置为项目根目录中的 moqui-1.5.3.war 之后apply
6.启动配置的 jar application(可debug启动)

7.浏览器登录地址 localhost:8080




